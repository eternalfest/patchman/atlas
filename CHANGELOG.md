# 0.3.1 (2024-03-18)

- **[Feature]** Add `immortal` property.
- **[Feature]** Add `noReward` property.
- **[Internal]** Update `Yarn` to 4.1.1; set `"preferUnplugged": true` for Yarn PnP.

# 0.3.0 (2023-02-03)

- **[Breaking change]** Rename and reorganize types:
  - `ds.LevelSetProperties` to `ds.WorldProperties`;
  - `RulesParser` to `RuleContext`;
  - `IPropType.parseDeclaration` to `parseRuleValue`; now takes `RulesContext` instead of raw `Zone`.
- **[Feature]** Add `disableTileCache` property.
- **[Feature]** Add `hide` option to `boss` property.
- **[Change]** Move documentation for the `Atlas.json` format to the README.
- **[Change]** Add `PropValues` helper class.
- **[Fix]** Fix infinite loops when using `ninjutsu` property with ninja + nightmare options.

# 0.2.0 (2021-11-13)

- **[Feature]** Add `noFireball` property.

# 0.1.1 (2021-10-24)

- **[Fix]** Fix typos in documentation.
- **[Fix]** `Atlas.json`: Fix default properties not working in worlds not mentioned in rules.

# 0.1.0 (2021-08-07)

- **[Feature]** Initial release
  - Provides `boss`, `darkness` & `ninjutsu` properties.
