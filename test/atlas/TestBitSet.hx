package atlas;

import atlas.ds.BitSet;

class TestBitSet {

  public static function testEmpty(): Void {
    var set = BitSet.empty();
    Test.assertEq(set.toString(), "{}");
    for(i in -100...100) {
      Test.assertEq(set[i], false);
    }
    Test.assertEq(set.isEmpty(), true);
  }

  public static function testSingleton(): Void {
    var set = BitSet.singleton(100);
    Test.assertEq(set.toString(), "{100}");
    for(i in -100...100) {
      Test.assertEq(set[i], i == 100);
    }
    Test.assertEq(set.isEmpty(), false);
  }

  public static function testFrom(): Void {
    Test.assertEq(BitSet.from([]).toString(), "{}");
    Test.assertEq(BitSet.from([5]).toString(), "{5}");
    Test.assertEq(BitSet.from([-50]).toString(), "{-50}");
    Test.assertEq(BitSet.from([10, -5, -85, 10, 50]).toString(), "{-85, -5, 10, 50}");
  }

  public static function testRange(): Void {
    Test.assertEq(BitSet.range(0, 0).toString(), "{}");
    Test.assertEq(BitSet.range(10, 0).toString(), "{}");
    Test.assertEq(BitSet.range(15, 20).toString(), "{15, 16, 17, 18, 19}");
    Test.assertEq(BitSet.range(30, 35).toString(), "{30, 31, 32, 33, 34}");
    Test.assertEq(BitSet.range(-20, -15).toString(), "{-20, -19, -18, -17, -16}");
    Test.assertEq(BitSet.range(-35, -30).toString(), "{-35, -34, -33, -32, -31}");
  }

  public static function testAddAndRemove(): Void {
    var vals = [42, -50, 6, 7, -5, 8, 75, 43, 9];
    var set = BitSet.empty();
    for (v in vals) {
      set.add(v);
      Test.assertEq(set.isEmpty(), false);
    }

    Test.assertEq(set.toString(), "{-50, -5, 6, 7, 8, 9, 42, 43, 75}");
    for (i in -100...100) {
      Test.assertEq(set[i], vals.indexOf(i) >= 0);
    }

    for (v in vals) {
      set.remove(v);
    }
    Test.assertEq(set.isEmpty(), true);
    Test.assertEq(set.toString(), "{}");
  }

  public static function testAddAndRemoveRange(): Void {
    var set = BitSet.empty();
    set.addRange(50, 100);
    set.removeRange(52, 96);
    set.addRange(60, 64);

    Test.assertEq(set.toString(), "{50, 51, 60, 61, 62, 63, 96, 97, 98, 99}");
  }

  public static function testAnd(): Void {
    function and(a: Array<Int>, b: Array<Int>, s: String): Void {
      Test.assertEq(BitSet.from(a).and(BitSet.from(b)).toString(), s);
      Test.assertEq(BitSet.from(b).and(BitSet.from(a)).toString(), s);
    }

    and([1, 2, 3], [4, 5, 6], "{}");
    and([25, 30, 35, 45], [35, 40, 45, 50], "{35, 45}");

    and([0, 33], [66, 99], "{}");
    and([0, 66], [33, 99], "{}");
    and([0, 33, 66], [33, 66, 99], "{33, 66}");
    and([0, 33, 66, 99], [33, 66], "{33, 66}");
  }

  public static function testOr(): Void {
    function or(a: Array<Int>, b: Array<Int>, s: String): Void {
      var a = BitSet.from(a);
      var b = BitSet.from(b);
      Test.assertEq(a.or(b).toString(), s);
      Test.assertEq(b.or(a).toString(), s);
      var a2 = a.copy();
      a2.addAll(b);
      Test.assertEq(a2.toString(), s);
      var b2 = b.copy();
      b2.addAll(a);
      Test.assertEq(b2.toString(), s);
    }

    or([1, 2, 3], [4, 5, 6], "{1, 2, 3, 4, 5, 6}");
    or([25, 30, 35, 45], [35, 40, 45, 50], "{25, 30, 35, 40, 45, 50}");
    or([0, 1, 2], [100, 101, 102], "{0, 1, 2, 100, 101, 102}");

    or([0, 33], [66, 99], "{0, 33, 66, 99}");
    or([0, 66], [33, 99], "{0, 33, 66, 99}");
    or([0, 33, 66], [33, 66, 99], "{0, 33, 66, 99}");
    or([0, 33, 66, 99], [33, 66], "{0, 33, 66, 99}");
  }

  public static function testCount(): Void {
    var set = BitSet.from([5, 10, 15, 16, 19, 25, 26]);

    set.addRange(-50, 5); 
    set.addRange(50, 100);
    set.addRange(300, 320);

    Test.assertEq(set.count(), 132);
  }

  public static function testMinMax(): Void {
    var set = BitSet.empty();
    Test.assertEq(set.minValue(), null);
    Test.assertEq(set.maxValue(), null);

    var set = BitSet.singleton(100);
    Test.assertEq(set.minValue(), 100);
    Test.assertEq(set.maxValue(), 100);

    var set = BitSet.range(100, 200);
    Test.assertEq(set.minValue(), 100);
    Test.assertEq(set.maxValue(), 199);
  }
}
