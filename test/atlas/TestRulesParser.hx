package atlas;

import atlas.ChunkResolver;
import atlas.Property;
import atlas.PropTypes;
import atlas.Zone;
import atlas.ds.BitSet;

class TestRulesParser {

  private static function makeChunkResolver(): ChunkResolver {
    var resolver = new ChunkResolver(null);

    var fakeLevel = "";
    for (i in 0...1000) fakeLevel += "a";

    @:privateAccess {
      resolver.addLevelSet(0, [for (i in 0...100) fakeLevel]);
      resolver.addLevelSet(1, [for (i in 0...200) fakeLevel]);
    }

    resolver.addTag({ name: "main", did: 0, lid: 0 });
    resolver.addTag({ name: "main_bis", did: 0, lid: 20 });
    resolver.addTag({ name: "dimA", did: 1, lid: 1 });
    resolver.addTag({ name: "dimB", did: 1, lid: 51 });
    resolver.addTag({ name: "dimC", did: 1, lid: 101 });
    resolver.addTag({ name: "dimD", did: 1, lid: 151 });
    
    return resolver;
  }

  public static function testParseSampleRules(): Void {
    var resolver = makeChunkResolver();
    var properties = [
      new Property().bridge("prop", PropTypes.INT),
    ];
    var raw: Dynamic = {
      ignoredTags: ["main_bis"],
      zones: {
        "named": { start: "dimB+10", end: "dimC" },
      },
      rules: ([{
        zone: { did: 0, start: 10, end: 20 },
      }, {
        zone: "main",
      }, {
        zone: ["named", "dimD"],
      }, {
        zone: "dimD+0",
      }, {
        zone: { did: 1, lid: [10, 15, 12, 11, 13, 14 ] },
      }]: Array<Dynamic>),
    };

    var rules = RulesContext.parseRules(raw, resolver, properties).rules;

    Test.assertEq(rules.length, 5);
    assertZoneRange(rules[0].zone, 0, 10, 21);
    assertZoneRange(rules[1].zone, 0, 0, 100);
    assertZoneRange(rules[2].zone, 1, 61, 200);
    assertZoneRange(rules[3].zone, 1, 151, 152);
    assertZoneRange(rules[4].zone, 1, 10, 16);
  }

  private static function assertZoneRange(zone: Zone, did: Int, start: Int, end: Int): Void {
    var isEmpty = true;
    zone.forEachWorld(function(actualDid, actualSet) {
      Test.assertEq(actualDid, did);
      var set = BitSet.range(start, end);
      Test.assertEq(actualSet.toString(), set.toString());
      isEmpty = false;
    });
    if (isEmpty) {
      throw "Expected non-empty zone";
    }
  }

}
