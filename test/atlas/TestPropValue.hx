package atlas;

import atlas.PropTypes;
import atlas.IPropType;
import atlas.errors.ParseError;

class TestPropValue {

  private static function makeRangeZone(start: Int, end: Int): Zone {
    var zone = new Zone();
    zone.addRange(0, start, end);
    return zone;
  }

  public static function testParseDeclBasic(): Void {
    var zone = new Zone();

    function assertParse<V>(type: IPropType<V>, raw: Dynamic, value: V): Void {
      var parsed = type.parseRuleValue(RulesContext.makeEmptyIn(zone), raw);
      Test.assertEq(parsed.getAt(0), value);
    }

    assertParse(PropTypes.BOOL, true, true);
    assertParse(PropTypes.BOOL, false, false);
    assertParse(PropTypes.INT, -420, -420);
    assertParse(PropTypes.FLOAT, 3.1415, 3.1415);
    assertParse(PropTypes.STRING, "hello!", "hello!");
  }

  public static function testParseInvalid(): Void {
    function assertError<V>(zone: Zone, type: IPropType<V>, raw: Dynamic): Void {
      try {
        type.parseRuleValue(RulesContext.makeEmptyIn(zone), raw);
        throw "Parsing didn't fail for: " + raw;
      } catch (e: ParseError) {
        // OK
      }
    }

    var zone = makeRangeZone(50, 100);

    assertError(zone, PropTypes.BOOL, null);
    assertError(zone, PropTypes.INT, null);
    assertError(zone, PropTypes.STRING, null);

    assertError(zone, PropTypes.BOOL, "not a bool");
    assertError(zone, PropTypes.INT, 3.1415);
    assertError(zone, PropTypes.INT, "not an int");

    assertError(zone, PropTypes.FLOAT, { nope: "nope" });
    assertError(zone, PropTypes.FLOAT, { start: 0, end: "nope" });
    assertError(zone, PropTypes.FLOAT, { start: 0, incr: "nope" });
    assertError(zone, PropTypes.FLOAT, { start: 0, step: 4.5 });
    assertError(zone, PropTypes.INT, { start: 0, end: 4.5 });
    assertError(zone, PropTypes.INT, { start: 0, incr: 4.5 });
    assertError(zone, PropTypes.INT, { start: 0, step: 4.5 });

    // Invalid ranged value for empty zone.
    assertError(new Zone(), PropTypes.FLOAT, { start: 0, end: 1 });

    // Invalid ranged value for non-contiguous zone.
    var zone2 = makeRangeZone(150, 200);
    zone2.addAll(zone);
    assertError(zone2, PropTypes.FLOAT, { start: 0, end: 10 });
  }

  public static function testParseLinear(): Void {
    var ctx = RulesContext.makeEmptyIn(makeRangeZone(50, 100));

    var val = PropTypes.FLOAT.parseRuleValue(ctx, { start: 0, end: 1 });
    Test.assertEq(val.getAt(50), 0);
    Test.assertEq(val.getAt(75), 0.5);
    Test.assertEq(val.getAt(100), 1);
    
    var val = PropTypes.FLOAT.parseRuleValue(ctx, { start: 1, end: 0 });
    Test.assertEq(val.getAt(50), 1);
    Test.assertEq(val.getAt(75), 0.5);
    Test.assertEq(val.getAt(100), 0);

    var val = PropTypes.INT.parseRuleValue(ctx, { start: 0, end: 10 });
    Test.assertEq(val.getAt(50), 0);
    Test.assertEq(val.getAt(74), 4);
    Test.assertEq(val.getAt(75), 5);
    Test.assertEq(val.getAt(100), 10);

    var val = PropTypes.INT.parseRuleValue(ctx, { start: 5, incr: 10 });
    Test.assertEq(val.getAt(50), 5);
    Test.assertEq(val.getAt(51), 15);
    Test.assertEq(val.getAt(60), 105);

    var val = PropTypes.INT.parseRuleValue(ctx, { start: 5, step: 10 });
    Test.assertEq(val.getAt(50), 5);
    Test.assertEq(val.getAt(55), 5);
    Test.assertEq(val.getAt(60), 6);
    Test.assertEq(val.getAt(70), 7);
  }
}
