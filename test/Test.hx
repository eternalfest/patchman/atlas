import haxe.io.Output;
import haxe.CallStack;

import haxe.macro.Expr;
import haxe.macro.ExprTools;

enum TestResult {
  Success;
  Failure(error: Dynamic);
}

class Test {

  private var name: String;
  private var fn: Void -> Void;

  public function new(name: String, fn: Void -> Void) {
    this.name = name;
    this.fn = fn;
  }

  public function run(): TestResult {
    try {
      this.fn();
      return Success;
    } catch(e: Dynamic) {
      return Failure(e);
    }
  }

  public static function runTestSuite(tests: Array<Test>, out: Output): Bool {
    var failures = 0;

    for (test in tests) {
      out.writeString('Running ${test.name}... ');
      switch (test.run()) {
        case Success:
          out.writeString("OK\n");
        case Failure(err):
          failures += 1;
          out.writeString("FAIL");
          for (line in formatError(err).split("\n")) {
            out.writeString("\n  ");
            out.writeString(line);
          }
          out.writeString("\n");
      }
    }

    out.writeString('Ran ${tests.length} tests with $failures failures\n');
    return failures == 0;
  }


  private static function formatError(err: Dynamic): String {
    var stack = CallStack.exceptionStack();
    return err + CallStack.toString(stack);
  }

  public static macro function assertEq(left: Expr, right: Expr): Expr {
    var msg = 'AssertionError: ${ExprTools.toString(left)} != ${ExprTools.toString(right)}';
    return macro @:pos(left.pos) {
      var __l = $left;
      var __r = $right;
      if (__l != __r) {
        throw $v{msg} + ":\n   left: " + __l + "\n  right: " + __r;
      }
    };
  }
}
