package atlas.ds;

/**
  A marker to indicate that a type has been erased.

  This is not enforced by Haxe's type system, but using `Erased`
  values in contravariant positions (e.g. function arguments) is
  dangerous and should be done with care.
**/
@:notNull
abstract Erased(Dynamic) {}
