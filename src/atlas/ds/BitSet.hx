package atlas.ds;

/**
  A bitset capable of holding arbitrary integers.
**/
abstract BitSet(Array<Int>) {

  private static inline var BITS_IN_WORD: Int = 32;
  private static inline var MASK: Int = -1 | 0;
  private static inline var HEADER_LEN: Int = 1;

  private static inline function wordIdx(val: Int, offset: Int): Int {
    return (val >> 5) + offset;
  }

  private static inline function wordBit(val: Int): Int {
    return val & 0x1F;
  }

  /**
    The bitset is stored as: `[offset, word1, word2, ...]`.

    The integer `v` is stored in the i'th bit of the j'th word, with:
      - `i = v & 0x1F`;
      - `j = (v >> 5) + offset`.

    This means that the minimum storable value is `(1 - offset) * 32`.

    Additionnally, the last word must be non-zero if the set isn't empty.
  **/
  private inline function new(raw: Array<Int>) {
    this = raw;
  }

  private inline function raw(): Array<Int> {
    return this;
  }

  private inline function offset(): Int {
    return this[0];
  }

  private inline function rawGet(word: Int, bit: Int): Bool {
    return (this[word] & (1 << bit)) != 0;
  }

  private inline function rawAdd(word: Int, bit: Int): Int {
    return this[word] |= (1 << bit);
  }

  private inline function rawRemove(word: Int, bit: Int): Int {
    return this[word] &= ~(1 << bit);
  }

  /**
    Creates a new set containing no values.
  **/
  public static inline function empty(): BitSet {
    return new BitSet([HEADER_LEN]);
  }

  /**
    Creates a new set containing a single element.
  **/
  public static inline function singleton(val: Int): BitSet {
    var set = empty();
    set.add(val);
    return set;
  }

  /**
    Creates a new set containing all elements in `[start, end)`.
    If `start >= end`, the set will be empty.
  **/
  public static inline function range(start: Int, end: Int): BitSet {
    var set = empty();
    set.addRange(start, end);
    return set;
  }

  /**
    Creates a new set containing the values of the given iterable.
  **/
  public static inline function from(vals: Iterable<Int>): BitSet {
    var set = empty();
    for (v in vals) {
      set.add(v);
    }
    return set;
  }

  /**
    Returns a copy of this set.
  **/
  public function copy(): BitSet {
    return new BitSet(this.copy());
  }

  /**
    Returns `true` if this set doesn't contain any elements.
  **/
  public inline function isEmpty(): Bool {
    return this.length == HEADER_LEN;
  }

  /**
    Returns the number of elements in the set. (**Warning**: has O(n) complexity)
  **/
  public function count(): Int {
    var i = HEADER_LEN;
    var len = this.length;
    var n = 0;
  
    while (i < len)
      n += BitTricks.countBits(this[i++]);

    return n;
  }

  private function ensureValid(wordNoOffset: Int): Int {
    var len = this.length;

    // The required word is past the end of the array.
    if (wordNoOffset > 0 && len > HEADER_LEN /* non-empty? */) {
      // Add new empty words as needed.
      // TODO: is this necessary on Flash?
      while (wordNoOffset >= len++) {
        this.push(0);
      }

      return wordNoOffset;
    }

    // We need to add a single empty word at the start.
    if (wordNoOffset == 0 || len == HEADER_LEN /* empty? */) {
      this[0] += HEADER_LEN - wordNoOffset;
      this.insert(HEADER_LEN, 0);
      return HEADER_LEN;
    }

    // We need to add several empty words at the start.
    var nbMissing = HEADER_LEN - wordNoOffset;
    this[0] += nbMissing;

    // TODO: this is O(n²), improve it!
    while(nbMissing-- > 0) {
      this.insert(HEADER_LEN, 0);
    }
    
    return HEADER_LEN;
  }

  /**
    Adds an element to the set.
  **/
  public function add(i: Int): Void {
    var word = wordIdx(i, offset());
    rawAdd(ensureValid(word), wordBit(i));
    // We don't remove any values, so no need to clean-up empty words.
  }

  /**
    Remove an element from the set.
  **/
  public function remove(i: Int): Void {
    var word = wordIdx(i, offset());
    var len = this.length;
    if (word < HEADER_LEN || word >= len) {
      // Out of bounds, nothing to do.
    } else {
      if (rawRemove(word, wordBit(i)) == 0 && word == --len) {
        // If the last word was set to zero, clean-up empty words at the end.
        do {
          this.pop();
        } while (this[--len] == 0);
      }
    }
  }

  /**
    Sets the membership status of an element.

    If `val` is `true`, the element will be added; else, it will be removed.  
    This returns `val` for compatibility with Haxe array syntax.
  **/
  @:arrayAccess
  public inline function set(i: Int, val: Bool): Bool {
    val ? add(i) : remove(i);
    return val;
  }

  /**
    Returns `true` if the element is present in the set.
  **/
  @:arrayAccess
  public function get(i: Int): Bool {
    var word = wordIdx(i, offset());
    if (word < HEADER_LEN || word >= this.length) {
      return false;
    } else {
      return rawGet(word, wordBit(i));
    }
  }

  /**
    Adds all values in `[start, end)` to the set.
    If `start >= end`, nothing happens.
  **/
  public function addRange(start: Int, end: Int): Void {
    if (start >= end)
      return;

    // Compute raw positions and expand the bitset if needed.
    var wordStart = ensureValid(wordIdx(start, offset()));
    var wordEnd = ensureValid(wordIdx(--end, offset()));

    // Add the range.
    var startMask = MASK << wordBit(start);
    var endMask = MASK >>> (BITS_IN_WORD - 1 - wordBit(end));
    if (wordStart == wordEnd) {
      this[wordStart] |= startMask & endMask;
    } else {
      // First word.
      this[wordStart++] |= startMask;

      // Middle words.
      while (wordStart < wordEnd) {
        this[wordStart++] = MASK;
      }

      // Last word.
      this[wordEnd] |= endMask;
    }

    // Adding a range can't remove values, so no need to clean-up empty words.
  }

  /**
    Remove all values in `[start, end)` from the set.
    If `start >= end`, nothing happens.
  **/
  public function removeRange(start: Int, end: Int): Void {
    if (start >= end)
      return;
    
    // Compute raw positions and clamp the range if needed.
    var off = offset();
    var len = this.length;
    var wordStart = wordIdx(start, off);
    if (wordStart < HEADER_LEN) {
      wordStart = HEADER_LEN;
      start = 0;
    } else {
      start = wordBit(start);
    }
    var wordEnd = wordIdx(--end, off);
    if (wordEnd >= len) {
      wordEnd = len - 1;
      end = 0;
    } else {
      end = BITS_IN_WORD - 1 - wordBit(end);
    }

    // Clear the range.
    var startMask = ~(MASK << start);
    var endMask = ~(MASK >>> end);
    var last = if (wordStart == wordEnd) {
      this[wordStart] &= startMask | endMask;
    } else {
      // First word.
      this[wordStart] &= startMask;

      // Middle words.
      while (++wordStart < wordEnd)
        this[wordStart] = 0;

      // Last word.
      this[wordEnd] &= endMask;
    };

    // If the last word was set to zero, clean-up empty words at the end. 
    if (last == 0 && wordEnd == --len) {
      do {
        this.pop();
      } while (this[--len] == 0);
    }
  }

  /**
    Sets the membership status of all values in `[start, end)`.
    If `start >= end`, nothing happens.
  **/
  public inline function setRange(start: Int, end: Int, val: Bool): Void {
    val ? addRange(start, end) : removeRange(start, end);
  }

  /**
    Adds all the elements of `other` to `this` set.
  **/
  public function addAll(other: BitSet): Void {
    // TODO: skip empty words at the start of other.

    // Ensure that the offset can support all of other's values.
    var otherOff = other.offset() - HEADER_LEN;
    var i = ensureValid(offset() - otherOff);
    var j = HEADER_LEN;

    // OR common values.
    var otherLen = other.raw().length;
    var len = Math.min(this.length, otherLen + i - j);    
    while (i < len) {
      this[i++] |= other.raw()[j++];
    }

    // Push the rest.
    while (j < otherLen) {
      this.push(other.raw()[j++]);
    }

    // We never remove values, so no need to clean-up empty words.
  }

  /**
    Returns the union of `this` set with `other`.
  **/
  public function or(other: BitSet): BitSet {
    var selfLen = this.length;
    var otherLen = other.raw().length;
    var delta = offset() - other.offset();

    // Swap the two bitsets so that delta is always positive,
    // with that `self` starting before `other`.
    var self: BitSet;
    if (delta < 0) {
      var tmp = selfLen;
      selfLen = otherLen;
      otherLen = tmp;
      self = other;
      other = new BitSet(this);
      delta = -delta;
    } else {
      self = new BitSet(this);
    }
    
    var i = HEADER_LEN + delta;
    // Copy the prefix.
    var union = self.raw().slice(0, i);

    if (i >= selfLen) {
      // There is no common range; we need to add empty words.
      while (i-- > selfLen) {
        union.push(0);
      }

      // Copy the whole `other` bitset.
      i = HEADER_LEN;
      while (i < otherLen) {
        union.push(other.raw()[i++]);
      }

    } else {
      // Union all words in the common range
      var end = Math.min(selfLen, otherLen + delta);
      do {
        union.push(self.raw()[i] | other.raw()[i - delta]);
      } while(++i < end);

      if (i < selfLen) {
        // Add trailing values from `self`.
        do {
          union.push(self.raw()[i]);
        } while (++i < selfLen);
      } else {
        // Add trailing values from `other`.
        i -= delta;
        while (i < otherLen) {
          union.push(other.raw()[i++]);
        }
      }
    }

    // OR-ing two bitsets can never remove values, so no need to clean-up empty words.
    return new BitSet(union);
  }

  /**
    Returns the intersection of `this` set and `other`.
  **/
  public function and(other: BitSet): BitSet {
    var thisOff = offset();
    var otherOff = other.offset();
    var delta = thisOff - otherOff;

    // Take the intersection of the range of both bitsets, i.e:
    // - the biggest lower bound (smallest offset);
    // - the smallest upper bound.
    var end = Math.min(this.length, other.raw().length + delta);
    var i: Int;
    var inter = if (delta < 0) {
      i = HEADER_LEN - 1;
      [thisOff];
    } else {
      i = delta + (HEADER_LEN - 1);
      [otherOff];
    };

    // TODO: elide the empty words at the start of the common range.

    // Intersect all words in the range.
    while (++i < end) {
      inter.push(this[i] & other.raw()[i - delta]);
    }

    // Clean-up empty words at the end.
    var i = inter.length;
    while (--i >= HEADER_LEN) {
      if (inter[i] != 0) break;
      inter.pop();
    }

    return new BitSet(inter);
  }

  /**
    Returns the minimum value in this set, of null if this set is empty.
  **/
  public function minValue(): Null<Int> {
    var idx = rawNextSetBit(HEADER_LEN * BITS_IN_WORD);
    if (idx == null) {
      return null;
    } else {
      return idx - offset() * BITS_IN_WORD;
    }
  }

  /**
    Returns the maximum value in this set, of null if this set is empty.
  **/
  public function maxValue(): Null<Int> {
    var l = this.length;
    if (l == HEADER_LEN)
      return null;

    // We know the last word is never zero.
    var last = BitTricks.leadingZeros(this[l-1]);
    return (l - offset()) * BITS_IN_WORD - (1 + last);
  }

  @:allow(atlas.ds.BitSetIterator)
  private function rawNextSetBit(idxNoOffset: Int): Null<Int> {
    var word = wordIdx(idxNoOffset, 0);

    var l = this.length;
    if (word >= l)
      return null;
    
    // Skip all empty words.
    var cur = this[word] & (MASK << wordBit(idxNoOffset));
    while (cur == 0) {
      cur = this[++word];
      if (word >= l)
        return null;
    }

    return word * BITS_IN_WORD + BitTricks.trailingZeros(cur);
  }

  /**
    Returns an iterator over the elements in the set.
    Elements are returned in increasing order.
  **/
  public inline function iterator(): BitSetIterator {
    return new BitSetIterator(
      new BitSet(this),
      offset() * BITS_IN_WORD,
      rawNextSetBit(HEADER_LEN * BITS_IN_WORD)
    );
  }

  /**
    Returns a user-friendly representation of this `BitSet`.
  **/
  public function toString(): String {
    var buf = new StringBuf();
    var sep = "{";

    for (i in iterator()) {
      buf.add(sep);
      buf.add(i);
      sep = ", ";
    }

    if (buf.length == 0) {
      return "{}";
    }
    
    buf.add("}");
    return buf.toString();
  }
}

class BitSetIterator {
  private var set: BitSet;
  private var offset: Int;
  private var idx: Null<Int>;

  @:allow(atlas.ds.BitSet)
  private inline function new(set: BitSet, offset: Int, idx: Int) {
    this.set = set;
    this.offset = offset;
    this.idx = idx;
  }

  public inline function hasNext(): Bool {
    return this.idx != null;
  }

  public inline function next(): Int {
    var v = this.idx;
    this.idx = this.set.rawNextSetBit(v + 1);
    return v - this.offset;
  }
}

// These are all heavily inspired from Java's Integer source code.
private class BitTricks {
  public static inline function trailingZeros(i: Int): Int {
    if (i == 0) return 32;
    var y, n = 31;
    y = i <<16; if (y != 0) { n -=16; i = y; }
    y = i << 8; if (y != 0) { n -= 8; i = y; }
    y = i << 4; if (y != 0) { n -= 4; i = y; }
    y = i << 2; if (y != 0) { n -= 2; i = y; }
    return n - ((i << 1) >>> 31); 
  }

  public static inline function leadingZeros(i: Int): Int {
    if (i == 0) return 32;
    var n = 1;
    if (i >>> 16 == 0) { n +=16; i <<=16; }
    if (i >>> 24 == 0) { n += 8; i <<= 8; }
    if (i >>> 28 == 0) { n += 4; i <<= 4; }
    if (i >>> 30 == 0) { n += 2; i <<= 2; }
    return n -= i >>> 31;
  }

  public static inline function countBits(i: Int): Int {
    // See: https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
    i -= (i >>> 1) & 0x55555555;
    i = (i & 0x33333333) + ((i >>> 2) & 0x33333333);
    return ((i + (i >>> 4) & 0xF0F0F0F) * 0x1010101) >>> 24;
  }
}
