package atlas.types;

import atlas.errors.ParseError;

/**
  A string property. See `PropTypes.STRING` for details.
**/
class StringType implements IPropType<String> {

  public function new() {}

  public function parseRuleValue(ctx: RulesContext, raw: Dynamic): IPropValue<String> {
    if (Std.is(raw, String)) {
      return PropValues.of(raw);
    }
    throw ParseError.expected(raw, "a string");
  }
}
