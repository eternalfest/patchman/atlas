package atlas;

/**
  An arbitrary Atlas property value of type `V`,
  that can vary depending on the level ID.

  See `PropValues` for common values to use.
**/
interface IPropValue<V> {
  function getAt(lid: Int): V;
}
