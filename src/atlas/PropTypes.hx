package atlas;

import atlas.types.BoolType;
import atlas.types.NumberType;
import atlas.types.StringType;

/**
  Common Atlas property types.
**/
class PropTypes {
  
  /**
    A boolean property.

    Accepts `true` and `false`.
  **/
  public static var BOOL(default, never): IPropType<Bool> = new BoolType();

  /**
    An integer property.

    Accepts integer constants, and varying integer values with the following
    syntax (only in contiguous zones):

    - `{ start: Int, end: Int }`  
      Equals `start` as the first level of the zone, `end` at the last level,
      and varies linearly between the two.
    - `{ start: Int, ?incr: Int, ?step: Int }`  
      Equals `start` as the first level of the zone, and increase by `incr`
      (`1` if not specified) every `step` levels (`1` if not specified).
  **/
  public static var INT(default, never): IPropType<Int> = new NumberType(TInt);

  /**
    A float property.

    Accepts float constants, and varying float values with the following
    syntax (only in contiguous zones):

    - `{ start: Float, end: Float }`  
      Equals `start` as the first level of the zone, `end` at the last level,
      and varies linearly between the two.
    - `{ start: Float, ?incr: Float, ?step: Int }`  
      Equals `start` as the first level of the zone, and increase by `incr`
      (`1` if not specified) every `step` levels (`1` if not specified).
  **/
  public static var FLOAT(default, never): IPropType<Float> = new NumberType(TFloat);

  /**
    A string property.

    Accepts any `String` constant.
  **/
  public static var STRING(default, never): IPropType<String> = new StringType();

}
