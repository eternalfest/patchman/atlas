package atlas;

import etwin.Error;
import etwin.ds.Set;
import etwin.ds.ReadOnlyMap;
import hf.mode.GameMode;
import hf.Data.LevelTag;

typedef ChunkInfo = {
  name: String,
  did: Int,
  lid: Int,
  size: Int,
};

/**
  Utility for deriving level chunks boundaries from raw level and tag data.

  Each tag (unless ignored with `ChunkResolver.ignoreTag`) is interpreted as the start of
  a new chunk. Each chunk may end with a single empty level, which won't count towards the
  chunk's size.
**/
class ChunkResolver {
  private var hf: hf.Hf;
  private var levels: Map<Int, Array<String>>;
  // If null, the tag name is ignored.
  private var tags: Map<String, Null<LevelTag>>;

  /**
    Creates an empty `ChunkResolver` for the given game.
  **/
  public function new(hf: hf.Hf) {
    this.hf = hf;
    this.levels = new Map();
    this.tags = new Map();
  }

  /**
    Returns the raw dimensions this resolver knows about.
  **/
  public inline function getLevels(): ReadOnlyMap<Int, Array<String>> {
    return this.levels;
  }

  /**
    Returns the number of levels in a dimension.

    Throws if the dimension isn't known by this resolver.
  **/
  public function getLevelSetSize(did: Int): Int {
    var world = this.levels[did];
    if (world == null)
      throw new Error("ChunkResolverError: unknown did " + did);
    return world.length;
  }

  private inline function addLevelSet(did: Int, levels: Array<String>): Void {
    if (this.levels.exists(did)) {
      throw new Error("ChunkResolverError: duplicate did " + did);
    }
    this.levels[did] = levels;
  }

  /**
    Loads into this resolver the level set `name`, with the given `did`.

    Throws if a dimension with the same did is already present.
  **/
  public function addWorld(did: Int, name: String): Void {
    // Create a SetManager to load the levels and verify the checksum.
    var world = this.hf.levels.SetManager._new(this.hf.GameManager.SELF, name);
    if (world.raw == null) {
      throw new Error("ChunkResolverError: couldn't load level set " + name);
    }

    addLevelSet(did, world.raw);
  }

  /**
    Loads into this resolver all dimensions in the given `GameMode`.

    Throws if a dimension with the same did is already present.
  **/
  public function addGameWorlds(game: GameMode): Void {
    for (did in 0...game.dimensions.length) {
      var world = game.dimensions[did];
      if (world != null && world.raw != null) {
        addLevelSet(did, world.raw);
      }
    }
  }

  /**
    Adds the given level tag to this resolver.

    Throws if a level tag with the same name is already present.
  **/
  public function addTag(tag: LevelTag): Void {
    var name = tag.name;
    if (this.tags.exists(name)) {
      if (this.tags[name] == null) {
        // The tag name is ignored, do nothing.
        return;
      }
      throw new Error("ChunkResolverError: duplicate tag name " + name);
    }
    
    this.tags[name] = tag;
  }

  /**
    Adds all the level tags of the current game.

    Throws if a level tag with the same name is already present, or if
    the game's tag list isn't loaded.
  **/
  public function addGameTags(): Void {
    var tags = this.hf.Data.LEVEL_TAG_LIST;
    if (tags == null) {
      throw new Error("ChunkResolverError: the game's tag list isn't loaded yet!");
    }
    for (tag in tags) {
      addTag(tag);
    }
  }

  /**
    Ignore the given tag name when computing level chunks.
  **/
  public function ignoreTag(name: String): Void {
    this.tags[name] = null;
  }

  /**
    Compute the list of level chunks using the loaded levels and tags.
  **/
  public function resolveChunks(): Array<ChunkInfo> {
    // Gather applicable chunks.
    var chunks = [];
    for (tag in this.tags) {
      // Skip ignored tags or tags that are in unknown dimensions.
      if (tag == null || this.levels[tag.did] == null)
        continue;
      chunks.push({
        name: tag.name,
        did: tag.did,
        lid: tag.lid,
        size: -1, // dummy value
      });
    }
    chunks.sort(ChunkResolver.compareChunks);
    if (chunks.length == 0)
      return chunks;

    // Add dummy chunk to signal the end of the array.
    chunks.push({
      name: "",
      did: chunks[chunks.length - 1].did + 1,
      lid: 0,
      size: -1,
    });

    // Compute chunks sizes.
    var cur = chunks[0];
    var curWorld = this.levels[cur.did];
    for (i in 1...chunks.length) {
      var next = chunks[i];
      var inSameWorld = cur.did == next.did;
      var end = inSameWorld ? next.lid : curWorld.length;

      cur.size = end - cur.lid;
      // Chunks may be separated by a single empty level, so we need to account for it.
      if (isEmptyLevel(curWorld[end - 1])) {
        cur.size--;
      }

      cur = next;
      if (!inSameWorld) {
        curWorld = this.levels[next.did];
      }
    }

    // Remove dummy chunk.
    chunks.pop();

    return chunks;
  }

  private static function compareChunks(left: ChunkInfo, right: ChunkInfo): Int {
    var cmp = left.did - right.did;
    if (cmp != 0) return cmp;
    cmp = left.lid - right.lid;
    if (cmp != 0) return cmp;
    return Reflect.compare(left.name, right.name);
  }

  private static inline var EMPTY_LEVEL_MAX_SIZE: Int = 717;
  private function isEmptyLevel(lvl: String): Bool {
    if (lvl.length < EMPTY_LEVEL_MAX_SIZE)
      return true;
    if (lvl.length > EMPTY_LEVEL_MAX_SIZE)
      return false;

    var codec = this.hf.PersistCodec._new();
    var lvl: hf.levels.Data = codec.decode(lvl);

    // See implementation of `hf.levels.SetManager.isEmptyLevel`.
    return lvl.__dollar__playerX == 0
      && lvl.__dollar__playerY == 0
      && lvl.__dollar__skinBg == 1
      && lvl.__dollar__skinTiles == 1
      && lvl.__dollar__badList.length == 0
      && lvl.__dollar__specialSlots.length == 0
      && lvl.__dollar__scoreSlots.length == 0;
  }
}
