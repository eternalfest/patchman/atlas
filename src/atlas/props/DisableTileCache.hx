package atlas.props;

import etwin.Obfu;
import patchman.IPatch;
import patchman.Ref;

/**
  Provides the `disableTileCache: Bool` property, preventing the tile cache from
  being used in the level.
**/
@:build(patchman.Build.di())
class DisableTileCache {

    public static var KEY(default, null): Property<Bool> = new Property();

    public static var PATCH(default, null): IPatch = Ref.auto(hf.levels.View.display)
    .before(function(hf, self, id) {
        var disableTileCache: Bool = Atlas.getProperty(self.world.game, KEY).or(_ => return);
        if (disableTileCache) {
            self.fl_cache = false;
        }
    });

    @:diExport
    public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("disableTileCache"), PropTypes.BOOL);

    @:diExport
    public var patch(default, null): IPatch = PATCH;

    public function new() {}
}
