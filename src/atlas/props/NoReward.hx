package atlas.props;

import etwin.Obfu;
import etwin.ds.Nil;
import patchman.IPatch;
import patchman.Ref;
import etwin.ds.FrozenArray;
import patchman.PatchList;

import atlas.errors.ParseError;

/**
  The possible values of the `noReward` Atlas property.
**/
@:enum
abstract NoRewardProp(String) {
  /**
    `false` or `"none"` - No disabling of crystals in this level.
  **/
  public var None = "none";
  /**
    `true` or `"small"` - Disables small crystals and diamonds.
  **/
  public var Small = "small";
  /**
    `"big"` - Disables big crystals.
  **/
  public var Big = "big";
  /**
    `"both"` - Disables small crystals and diamonds, as well as big crystals.
  **/
  public var Both = "both";
}

/**
  Provides the `noReward: NoRewardProp` property, preventing the enemies from
  dropping crystals or diamonds.
**/
@:build(patchman.Build.di())
class NoReward {

  public static var KEY(default, null): Property<NoRewardProp> = new Property();

  public static var PATCH(default, null): IPatch = new PatchList([
    Ref.auto(hf.entity.Bad.dropReward).prefix(function(hf, self) {
      return Atlas.getProperty(self.game, KEY)
      .map(prop => ((prop == Small || prop == Both) ? Nil.some(null) : Nil.none()));
    }),
    Ref.auto(hf.mode.Adventure.startLevel).after(function(hf, self) {
      var noReward = Atlas.getProperty(self, KEY);
      if (noReward == NoRewardProp.Big || noReward == NoRewardProp.Both) {
        self.perfectOrder.push(-1);
      }
    })
  ]);

  @:diExport
  public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("noReward"), new NoRewardPropType());

  @:diExport
  public var patch(default, null): IPatch = PATCH;

  public function new() {}
}

private class NoRewardPropType implements IPropType<NoRewardProp> {
  public function new() {}

  public function parseRuleValue(ctx: RulesContext, raw: Dynamic): IPropValue<NoRewardProp> {
    var value: NoRewardProp = if (Std.is(raw, Bool)) raw ? Small : None;
    else if (raw == Obfu.raw("none"))   None;
    else if (raw == Obfu.raw("small"))  Small;
    else if (raw == Obfu.raw("big"))    Big;
    else if (raw == Obfu.raw("both"))   Both;
    else {
      throw ParseError.expected(raw, "a boolean, or one of 'none', 'small', 'big', 'both'");
    };

    return PropValues.of(value);
  }
}
