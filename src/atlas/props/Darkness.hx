package atlas.props;

import etwin.Obfu;
import etwin.ds.Nil;
import patchman.IPatch;
import patchman.Ref;

/**
  Provides the `darkness: Float` property, specifying the amount of darkness
  in a level.

  If the property isn't defined, the darkness of the previously visited level
  will be used, with a slight reduction.

  **Warning:** This property is incompatible with `@patchman/game_params < 0.11.0`.
**/
@:build(patchman.Build.di())
class Darkness {

  public static var KEY(default, null): Property<Float> = new Property();

  public static var PATCH(default, null): IPatch = Ref.auto(hf.mode.GameMode.updateDarkness)
    .wrap(function(hf, self, old): Void {
      var darkness = self.forcedDarkness == null ? Atlas.getProperty(self, KEY) : Nil.none();
      var darkness = darkness.or(_ => {
        // Disable main world logic for darkness.
        var main: Bool = self.world.fl_mainWorld;
        self.world.fl_mainWorld = false;
        old(self);
        self.world.fl_mainWorld = main;
        return;
      });

      // Because we use forced darkness to simulate ambiant darkness, we need
      // to adjust the intensity depending on the lightbulb effects.
      for (p in self.getPlayerList()) {
        if (p.fl_torch || p.specialMan.actives[26])
          darkness *= 2/3; // 0.5/0.75 = (1/2)(4/3) = 2/3
      }

      self.forcedDarkness = darkness;
      old(self);
      self.forcedDarkness = null;
    });

  @:diExport
  public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("darkness"), PropTypes.FLOAT);

  @:diExport
  public var patch(default, null): IPatch = PATCH;

  public function new() {}
}
