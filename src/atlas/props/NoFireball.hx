package atlas.props;

import etwin.Obfu;
import patchman.IPatch;
import patchman.Ref;

/**
  Provides the `noFireball: Bool` property, preventing the fireball from
  appearing in the level.
**/
@:build(patchman.Build.di())
class NoFireball {

  public static var KEY(default, null): Property<Bool> = new Property();

  public static var PATCH(default, null): IPatch = Ref.auto(hf.mode.GameMode.main)
  .after(function(hf, self) {
    var noFireball: Bool = Atlas.getProperty(self, KEY).or(_ => return);
    if (noFireball) {
      self.huTimer = 0;
    }
  });

  @:diExport
  public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("noFireball"), PropTypes.BOOL);

  @:diExport
  public var patch(default, null): IPatch = PATCH;

  public function new() {}
}
