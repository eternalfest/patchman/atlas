package atlas.props;

import etwin.Obfu;
import etwin.ds.Nil;
import patchman.IPatch;
import patchman.Ref;

/**
  Provides the `ninjustu: Int` property, specifying the number of bads
  with a heart in Ninjutsu mode.

  If the property isn't defined, Ninjutsu mode will be completely
  disabled (no hearts and no skulls).
**/
@:build(patchman.Build.di())
class Ninjutsu {

  public static var KEY(default, null): Property<Int> = new Property();

  public static var PATCH(default, null): IPatch = Ref.auto(hf.mode.GameMode.onBadsReady)
    .replace(function(hf, self) {
      var bads = self.getBadClearList();
      if (!(self.fl_ninja && bads.length > 1))
        return;

      var friends: Float = Atlas.getProperty(self, KEY).or(_ => return);
      self.friendsLimit = 0;

      if (self.fl_nightmare) friends++;
      friends = Math.min(friends, bads.length - 1);

      var badClear = hf.Data.BAD_CLEAR;
      var bad: hf.entity.Bad = cast self.getOne(badClear);
      bad.fl_ninFoe = true;

      while (friends > 0) {
        bad = cast self.getOne(badClear);
        if (!(bad.fl_ninFriend || bad.fl_ninFoe)) {
          bad.fl_ninFriend = true;
          friends--;
        }
      }
    });

  @:diExport
  public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("ninjutsu"), PropTypes.INT);

  @:diExport
  public var patch(default, null): IPatch = PATCH;

  public function new() {}
}
