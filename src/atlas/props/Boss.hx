package atlas.props;

import etwin.Obfu;
import patchman.IPatch;
import patchman.Ref;
import patchman.PatchList;
import etwin.flash.MovieClip;
import etwin.ds.Nil;

import atlas.errors.ParseError;

/**
  The possible values of the `boss` Atlas property.
**/
@:enum
abstract BossProp(String) {
  /**
    `false` or `"none"` - No boss at this level.
  **/
  public var None = Obfu.raw("none");
  /**
    `"hide"` - Hidden boss at this level.
    
    This disallows warping but doesn't show the BOSS warning.
  **/
  public var Hide = Obfu.raw("hide");
  /**
    `true` or `"show"` - Full boss at this level.
    
    This disallows warping and shows the BOSS warning.
  **/
  public var Show = Obfu.raw("show");
}

/**
  Provides the `boss: BossProp` property, specifying which levels are BOSS levels.

  Note that `BAT_LEVEL` and `TUBERCULOZ_LEVEL` will always be treated
  as BOSS levels, even if the property isn't set.

  **Warning:** This property is incompatible with `@patchman/game_params < 0.11.0`.
**/
@:build(patchman.Build.di())
class Boss {

  public static var KEY(default, null): Property<BossProp> = new Property();

  public static var PATCH(default, null): IPatch = new PatchList([
    Ref.auto(hf.mode.Adventure.isBossLevel)
      .replace(function(hf, self, id): Bool {
        // WTF is going on here? copied from official code; should it be this?
        // super.isBossLevel() || (self.world.fl_mainWorld && (id == .. || id = ..))
        var isBoss = (self.world.fl_mainWorld || Ref.call(hf, super.isBossLevel, self, id))
          && (id == hf.Data.BAT_LEVEL || id == hf.Data.TUBERCULOZ_LEVEL);
        if (isBoss) return true;

        var props = Atlas.getWorldProperties(self.world);
        if (props != null) {
          props.getAt(id, KEY).map(prop => {
            return prop != None;
          });
        }
        return false;
      }),
    Ref.auto(hf.FxManager.attachWarning)
      .prefix(function(hf, self): Nil<MovieClip> {
        return Atlas.getProperty(self.game, KEY)
          .map(prop => (prop == Hide ? Nil.some(null) : Nil.none()));
      }),
  ]);

  @:diExport
  public var property(default, null): PropBridge = KEY.bridge(Obfu.raw("boss"), new BossPropType());

  @:diExport
  public var patch(default, null): IPatch = PATCH;

  public function new() {}
}

private class BossPropType implements IPropType<BossProp> {
  public function new() {}

  public function parseRuleValue(ctx: RulesContext, raw: Dynamic): IPropValue<BossProp> {
    var value: BossProp =
      if (Std.is(raw, Bool)) raw ? Show : None;
      else if (raw == Obfu.raw("none")) None;
      else if (raw == Obfu.raw("hide")) Hide;
      else if (raw == Obfu.raw("show")) Show;
      else {
      throw ParseError.expected(raw, "a boolean, or one of 'none', 'hide', 'show'");
    };

    return PropValues.of(value);
  }
}
