package atlas;

import etwin.ds.FrozenArray;
import etwin.ds.Nil;
import etwin.ds.WeakMap;
import etwin.Obfu;
import patchman.Ref;
import patchman.IPatch;
import patchman.DebugConsole;

import hf.levels.SetManager;
import hf.mode.GameMode;
import atlas.ds.WorldProperties;
import atlas.errors.ParseError;

/**
  Atlas mod: define level zones and attach properties to them.

  Zones and properties can be defined programatically, or in the `Atlas.json` data file.
**/
@:build(patchman.Build.di())
class Atlas {

  private static inline var DATA_NAME: String = Obfu.raw("Atlas");

  private static var WORLD_PROPS: WeakMap<SetManager, Null<WorldProperties>> = new WeakMap();

  /**
    Returns the properties attached to a world.
  **/
  public static inline function getWorldProperties(world: SetManager): Null<WorldProperties> {
    return WORLD_PROPS.get(world);
  }

  /**
    Overwrites the properties attached to a world.
  **/
  public static inline function setWorldProperties(world: SetManager, props: Null<WorldProperties>): Void {
    WORLD_PROPS.set(world, props);
  }

  /**
    Returns the value of a property at a given level, if it exists.
  **/
  public static function getPropertyAt<V>(game: GameMode, did: Int, lid: Int, prop: Property<V>): Nil<V> {
    var world = game.dimensions[did];
    if (world == null) return Nil.none();
    var props = getWorldProperties(world);
    if (props == null) return Nil.none();
    return props.getAt(lid, prop);
  }

  /**
    Returns the value of a property at the current level, if it exists.
  **/
  public static function getProperty<V>(game: GameMode, prop: Property<V>): Nil<V> {
    var props = getWorldProperties(game.world);
    if (props == null) return Nil.none();
    return props.getAt(game.world.currentId, prop);
  }

  @:diExport
  public var patch(default, null): IPatch;

  public function new(props: Array<PropBridge>, data: patchman.module.Data): Void {
    var data = data.get(DATA_NAME);

    this.patch = Ref.auto(GameMode.initGame).postfix(function(hf, self) {
      var rules = try {
        parseAtlasRules(self, props, data);
      } catch(e: ParseError) {
        DebugConsole.error("Failed to parse Atlas rules: " + e.toString());
        return;
      };

      loadAtlasProperties(self, rules);
    });
  }

  private static function parseAtlasRules(game: GameMode, props: Array<PropBridge>, raw: Dynamic): RulesContext.AtlasRules {
    var resolver = new ChunkResolver(game.root);
    resolver.addGameWorlds(game);
    resolver.addGameTags();
    return RulesContext.parseRules(raw, resolver, props);
  }

  private static function loadAtlasProperties(game: GameMode, rules: RulesContext.AtlasRules): Void {
    var byWorld = [for (did in rules.worlds) did => []];

    // Transforms the list of Atlas rules into a single property map for each world...
    for (rule in rules.rules) {
      rule.zone.forEachWorld(function(did, set) {
        var world = byWorld[did];
        patchman.Assert.debug(world != null);
        world.push({ zone: set, props: rule.props });
      });
    }

    // ...and attach the maps to the game.
    for (did in byWorld.keys()) {
      var world = game.dimensions[did];
      if (world != null) {
        setWorldProperties(world, new WorldProperties(
          FrozenArray.uncheckedFrom(byWorld[did]),
          rules.defaults
        ));
      }
    }
  }
}
