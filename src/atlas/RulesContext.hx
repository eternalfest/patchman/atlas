package atlas;

import etwin.Obfu;
import etwin.Error;
import etwin.ds.Set;
import hf.levels.SetManager;
import atlas.ChunkResolver.ChunkInfo;
import atlas.ds.PropertyMap;
import atlas.errors.ParseError;

typedef AtlasRules = {
  rules: Array<{ zone: Zone, props: PropertyMap }>,
  worlds: Set<Int>,
  ?defaults: PropertyMap,
};

/**
  The context when parsing Atlas configuration data.

  This is passed to `IPropTypes` when parsing a raw property value.
**/
class RulesContext {

  private static var EMPTY_ZONE: Zone = new Zone();

  private var chunkResolver: ChunkResolver;
  private var chunksByName: Map<String, ChunkInfo>;
  private var zonesByName: Map<String, Null<Zone>>;
  private var rawZonesByName: Dynamic;

  /**
    The zone definition we are currently inside of.
  **/
  public var zone(default, null): Zone = EMPTY_ZONE;

  private function new(chunkResolver: ChunkResolver) {
    this.chunkResolver = chunkResolver;
    this.chunksByName = [for (c in chunkResolver.resolveChunks()) c.name => c];
    this.zonesByName = new Map();
  }

  @:allow(atlas.TestPropValue)
  private static function makeEmptyIn(zone: Zone): RulesContext {
    var ctx = new RulesContext(new ChunkResolver(null));
    ctx.zone = zone;
    return ctx;
  }
  
  /**
    Parses Atlas rules from raw configuration data.

    The given `ChunkResolver` will be used to map tag names to zones, and
    properties will get parsed according to the list of `PropBridge`s.
  **/
  public static function parseRules(
    raw: Dynamic,
    chunkResolver: ChunkResolver,
    properties: Iterable<PropBridge>
  ): AtlasRules {
    var propMap = makeBridgeMap(properties);

    applyIgnoredTags(Obfu.field(raw, "ignoredTags"), chunkResolver);

    var parser = new RulesContext(chunkResolver);
    parser.parseNamedZones(Obfu.field(raw, "zones"));

    var rawRules = Obfu.field(raw, "rules");
    if (rawRules == null) rawRules = [];
    var rules = [for (rawRule in parseArray(rawRules)) {
      var zone = parser.parseZone(Obfu.field(rawRule, "zone"));
      parser.zone = zone;
      var props = parser.parsePropertyMap(rawRule, propMap);
      { zone: zone, props: props }
    }];

    var rawDefaults = Obfu.field(raw, "defaults");
    parser.zone = EMPTY_ZONE;
    var defaults = rawDefaults == null ? null : parser.parsePropertyMap(rawDefaults, propMap);

    var worlds = Set.from(chunkResolver.getLevels().keys());

    return { rules: rules, worlds: worlds, defaults: defaults };
  }

  private static function makeBridgeMap(properties: Iterable<PropBridge>): Map<String, PropBridge> {
    var propMap = new Map();
    for (prop in properties) {
      var name = prop.getName();
      if (propMap.exists(name))
        throw ParseError.custom(name, "duplicate property name");
      propMap[name] = prop;
    }
    return propMap;
  }

  private function parsePropertyMap(rawPropValues: Dynamic, props: Map<String, PropBridge>): PropertyMap {
    var rawProp: Dynamic = null;
    var propMap = new PropertyMap();

    var propNames = Reflect.fields(rawPropValues);
    propNames.remove(Obfu.raw("zone"));

    for (propName in propNames) {
      rawProp = Reflect.field(rawPropValues, propName);
      var propBridge = props.get(propName);
      if (propBridge == null)
        continue; // Ignore unknown properties.

      var parsed = propBridge.type.parseRuleValue(this, rawProp);
      propMap.set(propBridge.key, parsed);
    }

    return propMap;
  }

  private function parseNamedZones(rawZones: Dynamic): Void {
    if (rawZones == null)
      return;

    this.rawZonesByName = parseObject(rawZones);

    var fields = Reflect.fields(this.rawZonesByName);

    for (name in fields) {
      // TODO: check that zones names match \w+?

      if (this.chunksByName.exists(name))
        throw ParseError.custom(name, "zone can't have the same name as a chunk");
    }

    for (name in fields) {
      if (parseZoneByName(name) == null) {
        throw ParseError.custom(name, "unknown zone name");
      }
    }
  }

  private function parseZoneByName(name: String): Null<Zone> {
    var zone = this.zonesByName[name];
    if (zone == null) {
      if (this.zonesByName.exists(name))
        throw ParseError.custom(name, "named zone cycle detected");

      var rawZone = Reflect.field(this.rawZonesByName, name);
      if (rawZone == null)
        return null;

      this.zonesByName[name] = null;
      zone = parseZone(rawZone);
      this.zonesByName[name] = zone;
    }

    return zone;
  }

  private inline function parseZone(rawZone: Dynamic): Zone {
    // Avoid creating an extra object if this is a named zone.
    var zone = null;
    if (Std.is(rawZone, String)) {
      zone = parseZoneByName(rawZone);
    }

    if (zone == null) {
      zone = new Zone();
      parseZoneInto(rawZone, zone);
    }
    return zone;
  }

  private function parseZoneInto(rawZone: Dynamic, zone: Zone): Void {
    // `"foo"` and `"foo+5"` forms.
    if (Std.is(rawZone, String)) {
      var z = parseZoneByName(rawZone);
      if (z != null) {
        zone.addAll(z);
      } else {
        var tag = parseTagStr(parseString(rawZone));
        var chunk = tag.chunk;
        if (tag.offset == null) {
          zone.addRange(chunk.did, chunk.lid, chunk.lid + chunk.size);
        } else {
          zone.addLevel(chunk.did, chunk.lid + tag.offset);
        }
      }
      return;
    }

    // union form.
    if (Std.is(rawZone, Array)) {
      for (sub in (rawZone: Array<Dynamic>)) {
        parseZoneInto(sub, zone);
      }
      return;
    }

    var rawStart = Obfu.field(rawZone, "start");
    var rawEnd = Obfu.field(rawZone, "end");
    var rawDid = Obfu.field(rawZone, "did");

    // `{ start, end }` form.
    if (rawDid == null) {
      var start = parseTagStr(parseString(rawStart));
      var end = parseTagStr(parseString(rawEnd));

      var did = start.chunk.did;
      if (did != end.chunk.did)
        throw ParseError.custom(rawZone, "invalid zone definition: start and end must have the same did");

      var lidStart = start.chunk.lid + (start.offset == null ? 0 : start.offset);
      var lidEnd = end.chunk.lid + (end.offset == null ? end.chunk.size : (end.offset + 1)); // end is inclusive.

      zone.addRange(did, lidStart, lidEnd);
      return;
    }     
    
    // `{ did }`, `{ did, lid }` and `{ did, start, end }` forms.
    var did = parseInt(rawDid);
    var rawLid = Obfu.field(rawZone, "lid");
    if (rawLid == null) {
      if (rawStart == null && rawEnd == null) {
        zone.addRange(did, 0, this.chunkResolver.getLevelSetSize(did));
      } else if (rawStart != null && rawEnd != null) {
        zone.addRange(did, parseInt(rawStart), parseInt(rawEnd) + 1); // end is inclusive
      } else {
        throw ParseError.custom(rawZone, "invalid zone definition");
      }
    } else if (Std.is(rawLid, Array)) {
      for (lid in (rawLid: Array<Dynamic>)) {
        zone.addLevel(did, parseInt(lid));
      }
    } else {
      zone.addLevel(did, parseInt(rawLid));
    }

    // TODO: add intersection form?
  }

  private function parseTagStr(tagStr: String): { chunk: ChunkInfo, offset: Null<Int> } {
    var plus = tagStr.indexOf("+");
    var offset = null;
    if (plus >= 0) {
      var offStr = tagStr.substr(plus+1);
      tagStr = tagStr.substr(0, plus);

      // TODO: properly check that offStr matches `\d+`.
      offset = Std.parseInt(offStr);
      if (offset == null)
        throw ParseError.custom(tagStr, "invalid tag string");
    }

    var chunk = this.chunksByName[tagStr];
    if (chunk == null)
      throw ParseError.custom(tagStr, "unknown zone name");

    return { chunk: chunk, offset: offset };
  }

  private static function applyIgnoredTags(rawTags: Dynamic, chunkResolver: ChunkResolver): Void {
    if (rawTags == null)
      return;
    
    for (tag in parseArray(rawTags)) {
      chunkResolver.ignoreTag(parseString(tag));
    }
  }

  private static function parseString(raw: Dynamic): String {
    if (!Std.is(raw, String))
      throw ParseError.expected(raw, "a string");
    return raw;
  }

  private static function parseObject(raw: Dynamic): Dynamic {
    if (!Reflect.isObject(raw) || Std.is(raw, Array))
      throw ParseError.expected(raw, "an object");
    return raw;
  }

  private static function parseArray(raw: Dynamic): Array<Dynamic> {
    if (!Std.is(raw, Array))
      throw ParseError.expected(raw, "an array");
    return raw;
  }

  private static function parseInt(raw: Dynamic): Int {
    if (!Std.is(raw, Int) || raw < 0)
      throw ParseError.expected(raw, "a non-negative integer");
    return raw;
  }
}
